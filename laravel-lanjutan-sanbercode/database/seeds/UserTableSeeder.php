<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(
            // [
            //     'name' => "Super Admin",
            //     'email' => "super@admin.com",
            //     'password' => Hash::make('password'),
            //     'role_id' => 0,
            //     'created_at' => now(),

            // ],
            // [
            //     'name' => "Admin",
            //     'email' => "adminadmin@admin.com",
            //     'password' => Hash::make('password'),
            //     'role_id' => 1,
            //     'created_at' => now(),

            // ],
            [
                'name' => "Guest",
                'email' => "guest@guest.com",
                'password' => Hash::make('password'),
                'role_id' => 2,
                'created_at' => now(),

            ],
    );
    }
}
