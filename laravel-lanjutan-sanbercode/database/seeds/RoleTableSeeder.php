<?php

use Illuminate\Database\Seeder;

use Illuminate\Support\Facades\DB;

class RoleTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert(
            // [
            //     'name' => "Super Admin",
            //     'created_at' => now(),
            // ],
            // [
            //     'name' => "Admin",
            //     'created_at' => now(),

            // ],
            [
                'name' => "Guest",
                'created_at' => now(),

            ],
    );
    }
}
