<?php

namespace App\Http\Middleware;

use App\Role;
use Closure;
use Illuminate\Support\Facades\Auth;

class checkRole
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next, $role)
    {
        $user = Auth::user();

        if (!$user) {
            abort(403);

        }

        if ($role == 'routeSatu') {

            if ($user['role_id'] == 0) {
                return $next($request);
            }

        }

        if ($role == 'routeDua') {

            if (($user['role_id'] == 0) || ($user['role_id'] == 1)) {
                return $next($request);
            }
        }


        if ($role == 'routeTiga') {

            if (($user['role_id'] == 0) || ($user['role_id'] == 1) || ($user['role_id'] == 2)) {
                return $next($request);
            }
        }

        abort(403);


    }
}
