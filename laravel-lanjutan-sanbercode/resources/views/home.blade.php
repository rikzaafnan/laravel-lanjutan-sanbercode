@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                </div>
            </div>
        </div>
    </div>
    <div class="row justify-content-center">
        <div class="col-md-4">
            <div class="card">
                <div class="card-header">Route 1</div>

                <div class="card-body">
                    <a href={{url('/route-1')}} class="btn btn-primary">Route 1</a>
                </div>
            </div>
        </div>
        <div class="col-md-4">
            <div class="card">
                <div class="card-header">Route 2</div>

                <div class="card-body">
                    <a href={{url('/route-2')}} class="btn btn-primary">Route 2</a>
                </div>
            </div>
        </div>

        <div class="col-md-4">
            <div class="card">
                <div class="card-header">Route 3</div>

                <div class="card-body">
                <a href={{url('/route-3')}} class="btn btn-primary">Route 3</a>

                </div>
            </div>
        </div>
    </div>


</div>
@endsection
