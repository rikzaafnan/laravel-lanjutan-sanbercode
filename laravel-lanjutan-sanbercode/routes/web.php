<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/route-1', function() {
    return "route 1";
})->middleware('checkRole:routeSatu');

Route::get('/route-2', function() {
    return "route 2";
})->middleware('checkRole:routeDua');
Route::get('/route-3', function() {
    return "route 3";
})->middleware('checkRole:routeTiga');

Route::get('/data','TestController@index');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
