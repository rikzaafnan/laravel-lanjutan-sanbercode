console.log("1. Mengubah fungsi menjadi fungsi arrow")
const golden = () => {
    console.log("This is Golden")
}

golden()

console.log("\n")
console.log("2. Sederhanakan menjadi Object literal di ES6")

const newFunction = literal = (firstName, lastName) => {
    return {
        firstName,
        lastName,
        fullName : () => {
            console.log(firstName + " " + lastName) 
            return 
        }
    }
}
newFunction("William", "Imoh").fullName() 

console.log("\n")
console.log("3. Destructuring")

const newObject = {
    firstName: "Harry",
    lastName: "Potter Holt",
    destination: "Hogwarts React Conf",
    occupation: "Deve-wizard Avocado",
    spell: "Vimulus Renderus!!!"
  }

const { firstName, lastName, destination, occupation, spell } = newObject
console.log(newObject.firstName)

console.log("\n")
console.log("4. Array Spreading")
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west, ...east]
//Driver Code
console.log(combined)

console.log("\n")
console.log("5. Template Literals")
const planet = "earth"
const view = "glass"
var before = 'Lorem '+ view + 'dolor sit amet, ' +       'consectetur adipiscing elit,' + planet + 'do eiusmod tempor ' +     'incididunt ut labore et dolore magna aliqua. Ut enim' +     ' ad minim veniam'
// console.log(before)
let after = `Lorem ${view} dolor sit amet, consectetur adipiscing elit, ${planet} do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim vaniam`

console.log(after)