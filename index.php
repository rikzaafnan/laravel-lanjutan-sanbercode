<?php

    trait Hewan
    {
        public $nama;
        public $darah = 50;
        public $jumlahKaki;
        public $keahlian;

        public function atraksi($nama, $keahlian)
        {
            return $nama." sedang ".$keahlian.PHP_EOL;
        }
    }

    trait Fight
    {
        public $attackPower;
        public $defencePower;

        public function serang($serang, $diserang) {
            return $serang." sedang menyerang ".$diserang;
        }

        public function diserang($diserang,$darahAwal, $attackPower, $defencePower) {
            echo $diserang." sedang diserang sehingga darahnya sekarang = ". ($darahAwal - ($attackPower/$defencePower));
            echo "</br>";
        }
    }


    class Elang {

        use Hewan;
        use Fight;

        public function getInfoHewan($jumlahKaki, $keahlian, $attackPower, $deffencePower,$jenisHewan ) {
            echo "Jumlah Kaki bernilai ".$jumlahKaki.", ";
            echo "Keahlian bernilai ".$keahlian.", ";
            echo "attackPower = ".$attackPower.", ";
            echo "deffencePower = ".$deffencePower.", ";
            echo "Jenis Hewan adalah ".$jenisHewan.", ";
        }


    }

    class Harimau {
        use Hewan;
        use Fight;

        public function getInfoHewan($jumlahKaki, $keahlian, $attackPower, $deffencePower,$jenisHewan ) {
            echo "Jumlah Kaki bernilai ".$jumlahKaki.", ";
            echo "Keahlian bernilai ".$keahlian.", ";
            echo "attackPower = ".$attackPower.", ";
            echo "deffencePower = ".$deffencePower.", ";
            echo "Jenis Hewan adalah ".$jenisHewan.", ";

        }

    }


    echo"<b>Method yang ada di dalam kelas Hewan :</b>";
    echo"</br>";
    $elang = new Elang();
    echo $elang->atraksi("elang_1", "terbang tinggi");

    echo "</br>";
    $harimau = new Harimau();
    echo $harimau->atraksi("harimau_1", "lari cepat");

    echo "</br>";
    echo "</br>";
    echo"<b>Method yang ada di dalam kelas Fight : </b>";
    echo"</br>";
    echo $elang->serang("elang_1", "harimau_2");
    echo "</br>";
    echo $harimau->serang("harimau_3", "elang 3");
    
    echo "</br>";
    echo "</br>";
    echo $elang->diserang("harimau_3", $elang->darah,10,5);
    echo $harimau->diserang("elang_4", $harimau->darah,15,3);

    echo "</br>";
    echo "</br>";

    echo"<b>Method yang ada di dalam kelas Elang dan Harimau :</b>";
    echo"</br>";
    echo $elang->getInfoHewan(2, "terbang tinggi", 10, 5,"Elang");
    echo"</br>";
    echo $harimau->getInfoHewan(4, "lari cepat", 7, 8,"Harimau");
    
    // var_dump();

    // $contohHewan = new Hewan();
    // echo $contohHewan->atraksi("harimau_1", "lari cepat");
    // echo "</br>";
    // echo $contohHewan->atraksi("elang_1", "terbang tinggi");

    // echo "</br>";
    

    // var_dump($elang->atraksi("harimau_1","aaa"));

    
?>
